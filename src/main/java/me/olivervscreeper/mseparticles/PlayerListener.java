package me.olivervscreeper.mseparticles;

import me.olivervscreeper.mseparticles.effects.EffectHandler;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ItemFrame;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created on 23/10/2014.
 *
 * @author OliverVsCreeper
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onClick(PlayerInteractEntityEvent event){
        if(!(event.getRightClicked() instanceof ItemFrame)) return;
        ItemFrame frame = (ItemFrame) event.getRightClicked();
        if(frame.getItem().getType() == Material.AIR) return;
        if(MSEParticles.register.contains(event.getPlayer())){
            MSEParticles.register.remove(event.getPlayer());
            ItemStack i = frame.getItem();
            i = renameItem(i, ChatColor.GOLD + "Click for a Reward!");
            frame.setItem(i);
            event.setCancelled(true);
        }else {
            if(frame.getItem().getItemMeta().getDisplayName() == null || frame.getItem().getItemMeta().getDisplayName() == "") return;
            if (!frame.getItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD + "Click for a Reward!")) return;
            event.setCancelled(true);
            if(Vault.economy.getBalance(event.getPlayer().getName()) > 0){
                event.getPlayer().getInventory().addItem(renameItem(frame.getItem(), ChatColor.GOLD + "Reward (Click Me!)"));
                Vault.economy.withdrawPlayer(event.getPlayer().getName(), 1);
            }else{
                event.getPlayer().sendMessage(ChatColor.RED + "You'll need " + ChatColor.GOLD + "MSE Tokens" + ChatColor.RED + " for that!");
                event.getPlayer().sendMessage(ChatColor.RED + "Get them for 25p at the store: " + ChatColor.GOLD + "http://store.minecon.se/");
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.LEFT_CLICK_BLOCK)) return;
        if(event.getPlayer().getItemInHand() == null) return;
        ItemStack I = event.getPlayer().getItemInHand();
        if(I.getType().equals(Material.DIAMOND) && I.getEnchantments().containsKey(Enchantment.WATER_WORKER)){
            if(I.getAmount() == 1) {
                event.getPlayer().getInventory().remove(I);
            }else{
                event.getPlayer().getInventory().remove(I);
                I.setAmount(I.getAmount() - 1);
                event.getPlayer().getInventory().addItem(I);
            }
            event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.EAT, 10F, 1F);
            Vault.economy.depositPlayer(event.getPlayer().getName(), 1);
            event.getPlayer().sendMessage(ChatColor.GREEN + "+" + 1 + " MSE Token(s)! Balance: " + Vault.economy.getBalance(event.getPlayer().getName()));
        }else{
            EffectHandler.handleInteract(event.getPlayer(), I);
        }
    }

    public static ItemStack renameItem(ItemStack item, String name){
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(name);
        item.setItemMeta(im);
        return item;
    }

}
