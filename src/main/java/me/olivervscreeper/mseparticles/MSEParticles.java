package me.olivervscreeper.mseparticles;

import me.olivervscreeper.mseparticles.effects.EffectHandler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 23/10/2014.
 *
 * @author OliverVsCreeper
 */
public class MSEParticles extends JavaPlugin{

    public static List<Player> register = new ArrayList<Player>();
    public static Plugin plug;

    public void onEnable(){
        plug = this;
        Vault.setupEconomy();
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
        EffectHandler.loadEffects();
        this.getLogger().info("Particles ENABLED!");
        this.getLogger().info("Version licenced to MSE 2014.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        if(label.equalsIgnoreCase("registerreward")) {
            if (!sender.hasPermission("particles.register")) return true;
            register.add(p);
        }else if(label.equalsIgnoreCase("gettoken")){
            if (!sender.hasPermission("particles.spawn")) return true;
            if(args.length == 0){
                p.getInventory().addItem(getToken());
            }else if(args.length == 1){
                Vault.economy.depositPlayer(args[0], 1);
                if(Bukkit.getPlayer(args[0]) == null) return true;
                Bukkit.getPlayer(args[0]).sendMessage(ChatColor.GREEN + "+1 MSE Token! Balance: " + Vault.economy.getBalance(args[0]));
            }else if(args.length == 2){
                try{
                    Vault.economy.depositPlayer(args[0], Integer.parseInt(args[1]));
                    if(Bukkit.getPlayer(args[0]) == null) return true;
                    Bukkit.getPlayer(args[0]).sendMessage(ChatColor.GREEN + "+" + args[1] + " MSE Token! Balance: " + Vault.economy.getBalance(args[0]));
                }catch(Exception ex){
                    sender.sendMessage("Error.");
                }
            }
        }
        return true;
    }

    public ItemStack getToken(){
        ItemStack token = new ItemStack(Material.DIAMOND);
        token = PlayerListener.renameItem(token, ChatColor.GOLD + "MSE " + ChatColor.GRAY + "Token!");
        token.addUnsafeEnchantment(Enchantment.WATER_WORKER, 1);
        return token;
    }
}
