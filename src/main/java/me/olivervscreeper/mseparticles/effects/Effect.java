package me.olivervscreeper.mseparticles.effects;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created on 23/10/2014.
 *
 * @author OliverVsCreeper
 */
public abstract class Effect {

    public abstract boolean parseEffect(ItemStack I, Player p);

}
