package me.olivervscreeper.mseparticles.effects;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created on 23/10/2014.
 *
 * @author OliverVsCreeper
 */
public class TorchEffect extends Effect {
    @Override
    public boolean parseEffect(ItemStack I, Player p) {
        if(!I.getType().equals(Material.TRIPWIRE_HOOK)) return false;
        for(Block b : p.getLineOfSight(null, 200)){
            if(!b.getType().equals(Material.AIR)) return true;
            b.getWorld().playEffect(b.getLocation(), org.bukkit.Effect.MOBSPAWNER_FLAMES, 6);
            for(Player pl : Bukkit.getOnlinePlayers()){
                pl.playSound(p.getLocation(), Sound.FIREWORK_LAUNCH, 8F, 2F);
            }
        }
        return true;
    }
}
