package me.olivervscreeper.mseparticles.effects;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created on 23/10/2014.
 *
 * @author OliverVsCreeper
 */
public class TNTEffect extends Effect {
    @Override
    public boolean parseEffect(ItemStack I, Player p) {
        if(!I.getType().equals(Material.TNT)) return false;
        p.getWorld().createExplosion(p.getLocation(), 0F);
        for(int i = 0; i < 12; i++) {
            p.getWorld().playEffect(p.getLocation(), org.bukkit.Effect.SMOKE, 25, 5);
        }
        return true;
    }
}
