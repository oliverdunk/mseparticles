package me.olivervscreeper.mseparticles.effects;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created on 23/10/2014.
 *
 * @author OliverVsCreeper
 */
public class SugarEffect extends Effect implements Listener{
    @Override
    public boolean parseEffect(ItemStack I, Player p) {
        if(!I.getType().equals(Material.SUGAR)) return false;
        p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1200, 2));
        return true;
    }
}
