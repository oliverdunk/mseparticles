package me.olivervscreeper.mseparticles.effects;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created on 23/10/2014.
 *
 * @author OliverVsCreeper
 */
public class SquidEffect extends Effect implements Listener{


    @Override
    public boolean parseEffect(ItemStack I, Player p) {
        if(!I.getType().equals(Material.INK_SACK)) return false;
        final Entity e = p.getWorld().spawnEntity(p.getLocation(), EntityType.SQUID);
        p.setPassenger(e);
        return true;
    }

    @EventHandler
    public void onDeath(EntityDeathEvent event){
        if(!event.getEntity().getType().equals(EntityType.SQUID)) return;
        event.getDrops().clear();
    }
}
