package me.olivervscreeper.mseparticles.effects;

import me.olivervscreeper.mseparticles.MSEParticles;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 23/10/2014.
 *
 * @author OliverVsCreeper
 */
public class EffectHandler {

    public static List<Effect> effects = new ArrayList<Effect>();

    public static void loadEffects(){
        effects.add(new SquidEffect());
        Bukkit.getPluginManager().registerEvents(new SquidEffect(), MSEParticles.plug);
        effects.add(new TNTEffect());
        effects.add(new SugarEffect());
        Bukkit.getPluginManager().registerEvents(new SugarEffect(), MSEParticles.plug);
        effects.add(new TorchEffect());
    }

    public static void handleInteract(Player player, ItemStack i) {
        if(i == null) return;
        if(player == null) return;
        if(i.getItemMeta().getDisplayName() == null || i.getItemMeta().getDisplayName() == "") return;
        if(!i.getItemMeta().getDisplayName().equals(ChatColor.GOLD + "Reward (Click Me!)")) return;
        for(Effect effect : effects){
            if(!effect.parseEffect(i, player)) continue;
            if(i.getAmount() == 1) {
                player.getInventory().remove(i);
            }else{
                player.getInventory().remove(i);
                i.setAmount(i.getAmount() - 1);
                player.getInventory().addItem(i);
            }
        }
    }
}
